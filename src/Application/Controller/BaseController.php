<?php

namespace App\Application\Controller;

use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseController extends AbstractController
{
    public function repo($class): ObjectRepository
    {
        return $this->getDoctrine()->getRepository($class);
    }

    protected function param($param)
    {
        $request = $this->getRequestStack();

        return $request->request->get($param);
    }

    protected function query($param)
    {
        $request = $this->getRequestStack();

        return $request->query->get($param);
    }

    protected function header($param)
    {
        $request = $this->getRequestStack();

        return $request->headers->get($param);
    }

    private function getRequestStack(): Request
    {
        return $this->container->get('request_stack')->getMasterRequest();
    }
}
