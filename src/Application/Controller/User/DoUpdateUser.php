<?php
namespace App\Application\Controller\User;

use App\Application\Controller\BaseController;
use App\Domain\Decorator\TransactionDecorator;
use App\Domain\UseCase\DoUpdateUser\QueryUserByIdCommandHandler;
use App\Domain\UseCase\QueryUserById\QueryUserByIdCommand;
use App\Infrastructure\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoUpdateUser extends BaseController
{
    /**
     * @Route("/user/{id}", name="doUpdateUser", methods={"PUT","HEAD"}, requirements={"id"="[0-9]+")
     *
     * @param Request $request
     * @param int     $id
     *
     * @return array
     */
    public function doUpdateUser(Request $request, int $id): array
    {
        $managerRegistry = $this->getDoctrine();
        $objectManager = $this->getObjectManager();

        $command = new QueryUserByIdCommand($id);

        $commandHandler = new QueryUserByIdCommandHandler(new UserRepository($managerRegistry));

        $transaction = new TransactionDecorator($commandHandler, $objectManager);

        return $transaction->execute($command);
    }
}
