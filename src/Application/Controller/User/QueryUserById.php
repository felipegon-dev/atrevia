<?php

namespace App\Infrastructure\Controller\User;

use App\Application\Controller\BaseController;
use App\Domain\Decorator\TransactionDecorator;
use App\Domain\UseCase\DoUpdateUser\QueryUserByIdCommandHandler;
use App\Domain\UseCase\QueryUserById\QueryUserByIdCommand;
use App\Infrastructure\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class QueryUserById extends BaseController
{
    /**
     * @Route("/user/{id}", name="getUserById", methods={"GET","HEAD"})
     *
     * @param Request $request
     * @param $id
     *
     * @return array
     */
    public function getUserById(Request $request, int $id): array
    {
        $managerRegistry = $this->getDoctrine();
        $objectManager = $managerRegistry->getManager();

        $command = new QueryUserByIdCommand($id);

        $commandHandler = new QueryUserByIdCommandHandler(new UserRepository($managerRegistry));

        $transaction = new TransactionDecorator($commandHandler, $objectManager);

        return $transaction->execute($command);
    }
}
