<?php


namespace App\Application\Controller\Heartbeat;

use App\Application\Controller\BaseController;
use App\Infrastructure\Messages\Messages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoHeartbeat extends BaseController
{
    /**
     * @Route("/", name="index", methods={ "GET" })
     * @param Request $request
     * @return array
     */
    public function index()
    {
        return [Messages::OK];
    }
}
