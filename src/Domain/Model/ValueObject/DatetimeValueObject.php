<?php

namespace App\Domain\Model\ValueObject;

interface DatetimeValueObject extends ValueObject
{
    public function __construct(\DateTime $value);
}