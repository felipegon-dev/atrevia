<?php

namespace App\Domain\Model\ValueObject;

interface IntegerValueObject extends ValueObject
{
    public function __construct(int $value);
}