<?php

namespace App\Domain\EventHandler;

use App\Domain\Event\UserWasUpdated;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserWasUpdatedHandler implements MessageHandlerInterface
{
    // TODO: magic functions are bad practices
    public function __invoke(UserWasUpdated $userWasUpdated){
        // Do something...
        // Send some mails...
    }
}
