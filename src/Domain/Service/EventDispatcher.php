<?php

namespace App\Domain\Service;

use App\Domain\Model\DomainEvent;

interface EventDispatcher
{
    public function notify(DomainEvent ...$events): void;
}
