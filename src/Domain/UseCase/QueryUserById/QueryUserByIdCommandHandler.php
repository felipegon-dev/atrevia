<?php


namespace App\Domain\UseCase\DoUpdateUser;

use App\Application\Repository\UserRepository;
use App\Application\UseCase\DoUpdateUser\DoUpdateUserCommand;
use App\Domain\Model\User\User;
use App\Domain\Model\User\ValueObject\UserNameValueObject;
use App\Domain\Service\EventDispatcher;
use App\Domain\UseCase\CommandHandler;

class QueryUserByIdCommandHandler implements CommandHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EventDispatcher
     */

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function execute(DoUpdateUserCommand $command): User
    {
        return  $this->userRepository->findByIdOrFail($command->getId());
    }
}
