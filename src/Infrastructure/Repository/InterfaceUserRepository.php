<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\User\User;

interface InterfaceUserRepository
{
    public function findByIdOrFail($id): User;

    public function save(User $user): void;
}
